package com.tm.redis.message;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringRedisMessagingApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringRedisMessagingApplication.class, args);
	}

}
