 package com.tm.redis.message.repository;


import com.tm.redis.message.entity.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class EmployeeRepository {

    private static final String HASH_KEY = "employee";
    @Autowired
    RedisTemplate redisTemplate;

    public Employee save(Employee employee) {
        // employee representation in Redis
        // { id : 111, name : 'emp1', }
        redisTemplate.opsForHash().put(HASH_KEY, employee.getId(), employee);
        return employee;
    }

    public List<Employee> findAll() {
        return redisTemplate.opsForHash().values(HASH_KEY);
    }

    public Employee findById(int id) {
        return (Employee) redisTemplate.opsForHash().get(HASH_KEY, id);
    }

    public Employee update(Employee employee){
        redisTemplate.opsForHash().put(HASH_KEY, employee.getId(), employee);
        return employee;
    }

    public boolean delete(int id){
        redisTemplate.opsForHash().delete(HASH_KEY,id);
        return true;
    }
}
