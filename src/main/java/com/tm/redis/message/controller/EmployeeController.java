package com.tm.redis.message.controller;

import com.tm.redis.message.entity.Employee;
import com.tm.redis.message.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/employee-service")
public class EmployeeController {

    @Autowired
    private EmployeeService service;


    @PostMapping("employee")
    public ResponseEntity<?> saveEmployee(@RequestBody Employee employee) {
        ResponseEntity<?> responseEntity = null;

        try {
            Employee createdEmployee = this.service.save(employee);
            responseEntity = new ResponseEntity<>(createdEmployee, HttpStatus.CREATED);

        } catch (Exception e) {
            e.printStackTrace();
            responseEntity = new ResponseEntity<>("some internal error occured. Please try again!!!", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    @GetMapping("employee")
    public ResponseEntity<?> getEmployeeList() {

        try {
            return ResponseEntity.ok(this.service.findAll());
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.internalServerError().build();
        }
    }

    @GetMapping("employee/{id}")
    public ResponseEntity<?> getEmployeeById(@PathVariable("id") int id) {
        ResponseEntity<?> responseEntity = null;
        try {
            responseEntity = new ResponseEntity<>(this.service.findById(id), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            responseEntity = new ResponseEntity<>("some internal error occured. Please try again!!!", HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return responseEntity;
    }

    @PutMapping("employee")
    public ResponseEntity<?> updateEmployee(@RequestBody Employee employee) {
        ResponseEntity<?> responseEntity = null;

        try {
            Employee updatedEmployee = service.update(employee);
            responseEntity = new ResponseEntity<>(updatedEmployee, HttpStatus.CREATED);
        } catch (Exception e) {
            e.printStackTrace();
            responseEntity = new ResponseEntity<>("some internal error occured. Please try again!!!", HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return responseEntity;
    }

    @DeleteMapping("employee/{id}")
    public ResponseEntity<?> deleteEmployee(@PathVariable("id") int id) {
        ResponseEntity<?> responseEntity = null;
        try {
            responseEntity = new ResponseEntity<>(this.service.delete(id), HttpStatus.OK);
        } catch (Exception e) {
            responseEntity = new ResponseEntity<>("some internal error occured. Please try again!!!", HttpStatus.INTERNAL_SERVER_ERROR);

        }

        return responseEntity;
    }
}
