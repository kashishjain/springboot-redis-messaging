package com.tm.redis.message.service;

import com.tm.redis.message.entity.Employee;

import java.util.List;

public interface IEmployeeService {

    public Employee save(Employee employee);
    public List<Employee> findAll();
    public Employee findById(int id);
    public Employee update(Employee employee);
    public boolean delete(int id);
}
